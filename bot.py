import os
import discord
import random
from discord.ext import commands
from discord.utils import get

intents = discord.Intents(messages = True, guilds = True, reactions = True, members = True, presences = True)
client = commands.Bot(command_prefix = '?b ', intents = intents)


client.remove_command('help')

@client.event
async def on_ready():
    print('Bot is ready')

welcome_channel = 719266359766417589

member_count_channel = 708502130075500616

@client.event
async def on_member_join(member):
    guild = member.guild
    channel = get(guild.channels, id=welcome_channel)
    await channel.edit(name = f'Member Count: {guild.member_count}')
    channel = client.get_channel(member_count_channel)
    await channel.send(f'{member} has joined this server!')

@client.event
async def on_member_remove(member):
    guild = member.guild
    channel = get(guild.channels, id=welcome_channel)
    await channel.edit(name = f'Member Count: {guild.member_count}')
    channel = client.get_channel(member_count_channel)
    await channel.send(f'{member} has left this server')

@client.command()
async def ping(ctx):
    res = f'Pong! {round(client.latency * 1000)}ms '
    await ctx.send(res)

@client.command(aliases = ['8ball'])
async def _8ball(ctx,*, question=None):
    responses = ["It is certain.",
                "It is decidedly so.",
                "Yes - definitely.",
                "You may rely on it.",
                "As I see it, yes.",
                "Most likely.",
                "Outlook good.",
                "Yes.",
                "Signs point to yes.",
                "Reply hazy, try again.",
                "Ask again later.",
                "Better not tell you now.",
                "Cannot predict now.",
                "Concentrate and ask again.",
                "Don't count on it.",
                "My sources say no.",
                "Very doubtful."]
    embed = discord.Embed(
        title = f"Answer: {random.choice(responses)}",
        colour = discord.Colour.dark_grey()
    )
    embed.set_thumbnail(url='https://cdn.discordapp.com/attachments/785371195435778072/786127679161237524/image-removebg-preview.png')
    embed.set_author(name = f"Bacn's Magic 8ball")
    embed.add_field(name = 'Question:', value = f'{question}')
    await ctx.send(embed=embed)

@client.command()
@commands.has_permissions(manage_messages=True)
async def purge(ctx, amount = 5):
    await ctx.channel.purge(limit=amount)

@client.command()
@commands.has_permissions(kick_members=True)
async def kick(ctx, member : discord.Member, *, reason = None):
    embed = discord.Embed(
        title = f"Successively kicked {member}",
        colour = discord.Colour.orange()
    )
    embed.set_thumbnail(url='https://cdn.discordapp.com/attachments/785371195435778072/786127143473381376/image-removebg-preview_3.png')
    embed.set_author(name = f' Kicked {member}')
    await ctx.send(embed = embed)
    await member.kick(reason = reason)


@client.command()
@commands.has_permissions(ban_members=True)

async def ban(ctx, member : discord.Member, *, reason = None):
    embed = discord.Embed(
        title = f"Successively banned {member}",
        colour = discord.Colour.orange()
    )
    embed.set_thumbnail(url='https://cdn.discordapp.com/attachments/785371195435778072/786118442909827072/hqdefault_live.png')
    embed.set_author(name = f' Banned {member}')
    await ctx.send(embed = embed)
    await member.ban(reason = reason)


@client.command()
@commands.has_permissions(ban_members=True)
async def unban(ctx, *, member):
    banned_users = await ctx.guild.bans()
    member_name, member_discriminator = member.split("#")

    embed = discord.Embed(
        title = f"Successively unbanned {member}",
        colour = discord.Colour.orange()
    )
    embed.set_thumbnail(url='https://cdn.discordapp.com/attachments/785371195435778072/786118442909827072/hqdefault_live.png')
    embed.set_author(name = f' Unbanned {member}')

    for ban_entry in banned_users:
        user = ban_entry.user

        if(user.name, user.discriminator) == (member_name, member_discriminator):
            await ctx.guild.unban(user)
            await ctx.send(embed = embed)
            return

@client.command()
@commands.has_permissions(manage_messages=True)
async def mute(ctx, member : discord.Member, *, reason = None):
    guild = ctx.guild
    mutedRole = get(guild.roles, name = "Muted")

    if not mutedRole:
        mutedRole = await guild.create_role(name = "Muted")

        for channel in guild.channels:
            await channel.set_permission(mutedRole, speak = False, send_messages = False)

    await member.add_roles(mutedRole,reason = reason)

    embed = discord.Embed(
        title = f"Successively Muted {member} for {reason}",
        colour = discord.Colour.orange()
    )
    embed.set_thumbnail(url='https://cdn.discordapp.com/attachments/785371195435778072/786122799420145664/1f507.png')
    embed.set_author(name = f' Muted {member}')

    await ctx.send(embed = embed)

    embed2 = discord.Embed(
        title = f"You have been muted in {guild.name} for {reason}",
        colour = discord.Colour.gold()
    )
    embed2.set_thumbnail(url='https://cdn.discordapp.com/attachments/785371195435778072/786122799420145664/1f507.png')
    embed2.set_author(name = f'Muted in {guild.name}')

    await member.send(embed = embed2)


@client.command()
@commands.has_permissions(manage_messages=True)
async def unmute(ctx,member: discord.Member):
    guild = ctx.guild
    mutedRole = get(ctx.guild.roles, name = "Muted")
    await member.remove_roles(mutedRole)


    embed = discord.Embed(
        title = f"Successively Unmuted {member}",
        colour = discord.Colour.orange()
    )
    embed.set_thumbnail(url='https://cdn.discordapp.com/attachments/785371195435778072/786122799420145664/1f507.png')
    embed.set_author(name = f' Unmuted {member}')
    await ctx.send(embed=embed)

    embed2 = discord.Embed(
        title = f"You have been unmuted in {guild.name}",
        colour = discord.Colour.gold()
    )
    embed2.set_thumbnail(url='https://cdn.discordapp.com/attachments/785371195435778072/786122799420145664/1f507.png')
    embed2.set_author(name = f'Unmuted in {guild.name}')
    await member.send(embed = embed2)


@client.command()
async def Embed(ctx):
    embed = discord.Embed(
        title = "Title",
        description = "This is a description",
        colour = discord.Colour.orange()


    )

    embed.set_footer(text = "This is a footer.")
    embed.set_image(url='https://cdn.discordapp.com/attachments/785371195435778072/786116949020114954/unknown1.png')
    embed.set_thumbnail(url='https://cdn.discordapp.com/attachments/785371195435778072/786117038388412465/bbworld.png')
    embed.set_author(name = 'Author',
    icon_url = 'https://cdn.discordapp.com/attachments/785371195435778072/786117326420443146/MattheW.png')
    embed.add_field(name = 'Field', value = 'Field Value')
    await ctx.send(embed=embed)



client.run(os.environ["DISCORD_API_TOKEN_BOT"])
